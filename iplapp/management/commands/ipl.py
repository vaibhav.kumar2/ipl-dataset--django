from iplapp.models import Match, Delivery
from django.core.management.base import BaseCommand
from django.conf import settings
import os


class Command(BaseCommand):

    def handle(self, *args, **kwargs):
        # path = os.path.join(settings.BASE_DIR, 'ipldataset/csv_file/matches.csv')
        path = os.path.join(settings.BASE_DIR, 'ipldataset/csv_file/deliveries.csv')
        # insert_count = Match.objects.from_csv(path)
        insert_count = Delivery.objects.from_csv(path)
        print (f"{insert_count} records inserted")

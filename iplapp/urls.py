from django.urls import path
from . import views

urlpatterns = [
    path('iplapp/questions/',views.index, name = 'index'),
    path('iplapp/questions/one/', views.matches_per_year , name ='matches_per_year'),
    path('iplapp/questions/one/graph/', views.matches_per_year_graph , name ='matches_per_year_graph'),
    path('iplapp/questions/two/', views.team_won_ipl, name ='team_won_ipl'),
    path('iplapp/questions/two/graph/', views.team_won_ipl_graph , name ='team_won_ipl_graph'),
    path('iplapp/questions/three/', views.conceded_runs, name ='conceded_runs'),
    path('iplapp/questions/three/graph/', views.conceded_runs_graph, name ='conceded_runs_graph'),
    path('iplapp/questions/four/', views.bowler_economy, name ='bowler_economy'),
    path('iplapp/questions/four/graph', views.bowler_economy_graph, name ='bowler_economy_graph'),
    path('matches/', views.matches, name = 'matches'),
    path('matches/<match_id>',views.match_details, name = 'match_details'),
    path('deliveries/',views.deliveries, name='deliveries_details'),
    path('deliveries/<match_id>', views.deliveries_details, name = 'delivery_details'),
    path('match_list/', views.MatchList.as_view()),
    path('delivery_list/', views.DeliveryList.as_view()),

]


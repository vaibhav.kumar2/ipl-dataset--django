from django.shortcuts import render
from django.http import HttpResponse, JsonResponse
from django.db.models import Count, Sum, Case, When, FloatField, F
from django.db.models.functions import Cast
from .models import Match, Delivery
from django.views.decorators.cache import cache_page
from django.conf import settings
from django.core.cache.backends.base import DEFAULT_TIMEOUT
from django.views.decorators.cache import cache_page
from django.views.decorators.csrf import csrf_exempt
import json
from django.conf import settings
from rest_framework.views import APIView
from . serializer import MatchSerializer, DeliverySerializer
from django.core.paginator import Paginator
from rest_framework import generics


CACHE_TTL = getattr(settings, 'CACHE_TTL', DEFAULT_TIMEOUT)

def index(request):
    return render(request, 'iplapp/index.html')


@cache_page(CACHE_TTL)
def matches_per_year(request):
    matches_per_year = Match.objects.values('season').annotate(matches = Count('season')).order_by('season')
    matches_per_year = list(matches_per_year)
    return JsonResponse(matches_per_year, safe = False)

@cache_page(CACHE_TTL)
def team_won_ipl(request):
    team_won_ipl = Match.objects.values('season', 'winner').annotate(wins = Count('winner')).order_by('season')
    team_won_ipl = list(team_won_ipl)
    result = transform_team_won_ipl(team_won_ipl)
    return JsonResponse(result, safe = False)


@cache_page(CACHE_TTL)
def conceded_runs(request):
    extra_conceded_runs = Delivery.objects.values('bowling_team').filter(match_id__season=2016).\
                          annotate(extra_runs = Sum('extra_runs')).order_by('extra_runs')
    extra_conceded_runs = list(extra_conceded_runs)
    return JsonResponse(extra_conceded_runs, safe = False)


@cache_page(CACHE_TTL)
def bowler_economy(request):
    bowler_economy = Delivery.objects.filter(match_id__season = 2015, is_super_over = False).\
                    values('bowler').annotate(runs = Sum('batsman_runs') + Sum('wide_runs') + \
                    Sum('noball_runs')).annotate(balls = Count('ball')- \
                    Count(Case(When(noball_runs__gt = 0, then = 1)))-\
                    Count(Case(When(wide_runs__gt = 0, then = 1)))).\
                    annotate(economy = Cast((F('runs')/(F('balls')/6.0)),\
                    FloatField())).order_by('economy')[:10]
    bowler_economy = list(bowler_economy)
    result = transform_bowler_economy(bowler_economy)
    return JsonResponse(result, safe = False)


#transform data
def transform_bowler_economy(bowler_economy):
    teams = {}
    for bowler in bowler_economy:
        name = bowler['bowler']
        economy = bowler['economy']
        if economy in teams:
            teams += economy
        else:
            teams[name] =  economy
    return teams


def transform_team_won_ipl(team_won_ipl):
    matches = {}
    for match in team_won_ipl:
        match_winner_team = match['winner']
        season = match['season']
        wins = match['wins']
        if match_winner_team in matches:
            if season in matches[match_winner_team]:
                matches[match_winner_team][season] += wins
            else:
               matches[match_winner_team][season] = wins
        else:
           matches[match_winner_team] = {season : wins}
    return matches


#Plot Graph
def matches_per_year_graph(request):
    return render(request, 'iplapp/matches_per_year.html')

def team_won_ipl_graph(request):
    return render(request, 'iplapp/team_won_ipl.html')

def conceded_runs_graph(request):
    return render(request, "iplapp/conceded_runs.html")

def bowler_economy_graph(request):
    return render(request, "iplapp/bowler_economy.html")

#APIs
@csrf_exempt
def matches(request):
    if request.method == 'GET':
        matches = {}
        no_of_matches = list(Match.objects.all().values())
        matches['data'] = no_of_matches
        return JsonResponse(matches, safe = False)

    if request.method == 'POST':
        data = request.body.decode(encoding = 'utf-8')
        data = json.loads(data)
        match_id = len(Match.objects.all())+1
        matches = Match(id=match_id, season=data['season'],
        city=data['city'], date=data['date'],
        team1=data['team1'], team2=data['team2'],
        toss_winner=data['toss_winner'],
        toss_decision=data['toss_decision'],
        result=data['result'], dl_applied=data['dl_applied'],
        winner=data['winner'], win_by_runs=data['win_by_runs'],
        win_by_wickets=data['win_by_wickets'],
        player_of_match=data['player_of_match'],
        venue=data['venue'], umpire1=data['umpire1'],
        umpire2=data['umpire2'], umpire3=data['umpire3'])
        matches.save()
        return JsonResponse({'status': 'New Match added'}, status=200)

@csrf_exempt
def match_details(request, match_id):
    if request.method == 'GET':
        match_details = Match.objects.filter(id=match_id).values()
        match_details = list(match_details)
        return JsonResponse(match_details, safe = False)

    if request.method == 'PUT':
        data = request.body.decode(encoding = 'utf-8')
        data = json.loads(data)
        matches = Match.objects.filter(id = match_id)
        matches.update(**data)
        return JsonResponse({'status': 'Match updated'}, status=200)

    if request.method == 'DELETE':
        data = request.body.decode(encoding = 'utf-8')
        data = json.loads(data)
        matches = Match.objects.filter(id = match_id)
        matches.delete()
        return JsonResponse({'status':'deleted'}, status = 200)

@csrf_exempt
def deliveries(request):
    if request.method == 'GET':
        deliveries = {}
        no_of_deliveries = Delivery.objects.all().values()
        no_of_deliveries = list(no_of_deliveries)
        deliveries['data'] = no_of_deliveries
        return JsonResponse(deliveries, safe = False)

    if request.method == 'POST':
        data = request.body.decode(encoding = 'utf-8')
        data = json.loads(data)
        deliveries = Delivery(match_id=data['match_id'],inning=data['inning'],batting_team=data['batting_team'],bowling_team=data['bowling_team'],over=data['over'],ball=data['ball'],batsman=data['batsman'],non_striker=data['non_striker'],bowler=data['bowler'],is_super_over=data['is_super_over'],wide_runs=data['wide_runs'],bye_runs=data['bye_runs'],legbye_runs=data['legbye_runs'],noball_runs=data['noball_runs'],penalty_runs=data['penalty_runs'],batsman_runs=data['batsman_runs'],extra_runs=data['extra_runs'],total_runs=data['total_runs'],player_dismissed=data['player_dismissed'],dismissal_kind=data['dismissal_kind'],fielder=data['fielder'])
        deliveries.save()
        return JsonResponse({"status":"deliveries Added"}, status=200)

@csrf_exempt
def deliveries_details(request, match_id):
    if request.method == 'GET':
        deliveries_details = Delivery.objects.filter(id=match_id).values()
        deliveries_details = list(deliveries_details)
        return JsonResponse(deliveries_details, safe = False)
    if request.method == 'PUT':
        data = request.body.decode(encoding = 'utf-8')
        data = json.loads(data)
        deliveries = Delivery.objects.filter(id = match_id)
        deliveries.update(**data)
        return JsonResponse({'status': 'Delivery updated'}, status=200)

    if request.method == 'DELETE':
        data = request.body.decode(encoding = 'utf-8')
        data = json.loads(data)
        deliveries = Delivery.objects.filter(id = match_id)
        deliveries.delete()
        return JsonResponse({'status':'deleted'}, status = 200)

#REST APIs
class MatchList(generics.ListCreateAPIView):
    queryset = Match.objects.all()
    serializer_class = MatchSerializer

class DeliveryList(generics.ListCreateAPIView):
    queryset = Delivery.objects.all()
    serializer_class = DeliverySerializer




<h3>Ipl Data Project using Django</h3>
<b>Description</b>:
This data assignment you will transform raw data from IPL into graphs that will convey some meaning / analysis. For each part of this assignment you will have 2 parts -
Dataset: Download both csv files from https://www.kaggle.com/manasgarg/ipl
<br>

<b>Prequisites</b>: The development for this project requires several packages. Overview is as follows:

    1. We will use a virtual environment for resolving our project's dependencies.
    2. Create a virtualenv for your project using "virtualenv -p your/python3/path my_project"
    3. Activate this virtualenv using "source ~/.virtualenv/venvname/bin/activate" from your root.
    4. All the libraries installed in this virtual environment.
    5. Packages/libraries in the virtual environment are installed using pip install pip install package/library name
    6. Install Django and Django REST framework into the virtual environment.
    7. Set up a new project with a single application.


1. Create API route for all below questions
2. Plot for all questions using Highchart
Generate the following plots ...

    1. Plot the number of matches played per year of all the years in IPL.
    2. Plot a stacked bar chart of matches won of allvteams over   all the years of IPL.
    3. For the year 2016 plot the extra runs conceded per team.
    4. For the year 2015 plot the top economical bowlers.


Cache Using Redis
Specificaion:
Python 3.7
Database: PostgreSQL
IDE- VS Code
For plotting: Highchart